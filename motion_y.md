# Model - Y-Axis

The Y-axis is modelled as follows:

```mermaid
flowchart LR

    Motor --> Pulley --> Ballscrew --> Gantry
```

# Equations of force and motion

The below equations describe force and motion as they flow through the
components of the Y-axis.

## Motor #######################################################################

$M_{drive} - M_{inertia} - M_{out} = 0$

where:

- $M_{drive}$:   Moment generated by motor at given rotational velocity as
                 determined by motor torque curve.
- $M_{inertia}$: Moment result of motor's moment of inertia resisting
                 acceleration.
- $M_{out}$:     Moment transmitted to the pulley

with:

$M_{inertia} = I_{motor} \alpha_{motor}$

where:

- $I_{motor}$:      Moment of inertia for motor from datasheet [$rad/s^2$].
- $\alpha_{motor}$: Rotational acceleration of motor.

Leading to a function for the motor output from an input:

> **Moment:**
>
> $M_{motor\ out} = M_{motor\ drive} - M_{motor\ inertia}$
>
> **Velocity:**
>
> $\omega_{motor\ out} = \omega_{motor\ drive}$
>
> **Acceleration:**
>
> $\alpha_{motor\ out} = \alpha_{motor\ drive}$

## Pulley ######################################################################

$\frac{M_{in}}{r_{in}} - F_{friction} - \frac{M_{out}}{r_{out}} = 0$

dimensional analysis:

$\frac{[Nm]}{[m]} -[N] - \frac{[Nm]}{[m]} = 0$

where:

- $M_{in}$:       Moment going into the pulley system from the motor.
- $r_{in}$:       Radius (or teeth count, as it's the ratio that matters) of the
                  pulley on the motor.
- $F_{friction}$: Force of friction causing loss to the system.
- $r_{out}$:      Radius (or teeth count, as it's the ratio that matters) of the
                  pulley on the ballscrew.
- $M_{out}$:      Moment that will be transmitted into the ballscrew.

Where we approach $F_{friction}$ as an efficiency factor $\eta$ on force
transmitted:

$F_{friction} = \frac{\eta_{pulleys} M_{in}}{r_{in}}$

where:

- $\eta_{pulleys}$: The efficiency of the pulley transmission.

Leading to a function for the pulley output from an input:

> **Moment:**
>
> $M_{pulley\ out} = M_{pulley\ drive} (1 - \eta_{pulleys}) \frac{r_{pulley\ out}}{r_{pulley\ in}}$
>
> **Velocity:**
>
> $\omega_{pulley\ out} = \omega_{pulley\ drive} * \frac{r_{pulley\ in}}{r_{pulley\ out}}$
>
> **Acceleration:**
>
> $\alpha_{pulley\ out} = \alpha_{pulley\ drive} * \frac{r_{pulley\ in}}{r_{pulley\ out}}$

## Ballscrew ###################################################################

$M_{in} - M_{inertia} - M_{out} = 0$

where:

- $M_{in}$:      Moment transmitted onto the ballscrew from the pulley.
- $M_{inertia}$: Moment result of ballscrew's moment of inertia resisting
                 acceleration.
- $M_{out}$:     Moment transmitted to the ballnut to result in linear force
                 applied to gantry.

with:

$M_{inertia} = I_{ballscrew} \alpha_{ballscrew}$

where:

- $I_{ballscrew}$:      Moment of inertia for ballscrew as calculated [$rad/s^2$].
- $\alpha_{ballscrew}$: Rotational acceleration of ballscrew. 

Where the moment of inertia is approached a simple cylinder:

$I_{ballscrew} = \frac{mr^2}{2}$

where:

- $r$: Radius of the cylinder [m].
- $m$: Mass of the cylinder [kg].

Leading to a function for the ballscrew output from an input:

> **Moment:**
>
> $M_{ballscrew\ out} = M_{ballscrew\ in} - M_{inertia}$
>
> **Velocity:**
>
> $\omega_{ballscrew\ out} = \omega_{ballscrew\ in}$
>
> **Acceleration:**
>
> $\alpha_{ballscrew\ out} = \alpha_{ballscrew\ in}$

## Gantry ######################################################################

$F_{in} - F_{cutting } - F_{acceleration} = 0$

where:

$F_{in} = \eta_{ballscrew} * \frac{M}{\ell}$

with:

- $\eta_{ballscrew}$: Ballscrew efficiency
- $M$:                $M_{out}$ of the ballscrew
- $\ell$:             Ballscrew lead [$m/rad$]

and

$F_{acceleration} = m_{gantry} * a_{gantry}$

with:

- $F_{acceleration}$: Resultant force of acceleration on the gantry.
- $m_{gantry}$:       The mass of the gantry.
- $a_{gantry}$:       Acceleration of the gantry.

Leading to a function for the gantry output from input:

> **Force:**
>
> $F_{ballscrew} = M_{ballscrew} * \frac{\eta_{ballscrew}}{\ell_{ballscrew}}$
>
> **Velocity:**
>
> $v_{gantry} = \omega_{ballscrew} * \ell_{ballscrew}$
>
> **Acceleration:**
>
> $a_{gantry} = \alpha_{ballscrew} * \ell_{ballscrew}$

# Putting it all together ######################################################

We can now express $\alpha_{motor}$ and $\alpha_{ballscrew}$ as a function of
$a_{gantry}$ to allow us to solve the equation for acceleration:

$\alpha_{ballscrew} = \frac{a_{gantry}}{\ell_{ballscrew}}$

$\alpha_{motor} = \alpha_{ballscrew} * \frac{r_{pulley\ out}}{r_{pulley\ in}}$

$\alpha_{motor} = \frac{a_{gantry}}{\ell_{ballscrew}} * \frac{r_{pulley\ out}}{r_{pulley\ in}}$

So that we can express the rotational inertia terms in the motor and ballscrew
as a function of gantry acceleration:

$M_{inertia\ ballscrew} = I_{ballscrew} * \frac{a_{gantry}}{\ell_{ballscrew}}$

$M_{inertia\ motor} = I_{motor} * \frac{a_{gantry}}{\ell_{ballscrew}} * \frac{r_{pulley\ out}}{r_{pulley\ in}}$

Which finally allows us to write the force of the ballscrew acting on the gantry
as a function of motor moment by expanding step by step:

$F_{ballscrew} - F_{cutting} = m_{gantry} * a_{gantry}$

$M_{ballscrew} * \frac{\eta_{ballscrew}}{\ell_{ballscrew}} - F_{cutting} = m_{gantry} * a_{gantry}$

$(M_{ballscrew\ in} - M_{inertia\ ballscrew}) * \frac{\eta_{ballscrew}}{\ell_{ballscrew}} - F_{cutting} = m_{gantry} * a_{gantry}$

$(M_{ballscrew\ in} - I_{ballscrew} * \frac{a_{gantry}}{\ell_{ballscrew}}) * \frac{\eta_{ballscrew}}{\ell_{ballscrew}} - F_{cutting} = m_{gantry} * a_{gantry}$

$(M_{pulley\ drive} * (1 - \eta_{pulleys}) \frac{r_{pulley\ out}}{r_{pulley\ in}} - I_{ballscrew} * \frac{a_{gantry}}{\ell_{ballscrew}}) * \frac{\eta_{ballscrew}}{\ell_{ballscrew}} - F_{cutting} = m_{gantry} * a_{gantry}$

$((M_{motor\ drive} - M_{motor\ inertia}) * (1 - \eta_{pulleys}) \frac{r_{pulley\ out}}{r_{pulley\ in}} - I_{ballscrew} * \frac{a_{gantry}}{\ell_{ballscrew}}) * \frac{\eta_{ballscrew}}{\ell_{ballscrew}} - F_{cutting} = m_{gantry} * a_{gantry}$

$((M_{motor\ drive} - I_{motor} * \frac{a_{gantry}}{\ell_{ballscrew}} * \frac{r_{pulley\ out}}{r_{pulley\ in}}) * (1 - \eta_{pulleys}) \frac{r_{pulley\ out}}{r_{pulley\ in}} - I_{ballscrew} * \frac{a_{gantry}}{\ell_{ballscrew}}) * \frac{\eta_{ballscrew}}{\ell_{ballscrew}} - F_{cutting} = m_{gantry} * a_{gantry}$

Expanding this equation:

