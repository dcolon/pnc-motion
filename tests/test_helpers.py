import pytest
from unittest import TestCase
import pncmotion.helpers as helpers

class HelpersTest(TestCase):
    def test_calculate_cylinder_mass(self):
        """
        Tests calculation of cylinder mass.
        Uses https://calculator.academy/cylinder-mass-calculator/ to generate
        test cases.
        """
        assert helpers.calculate_cylinder_mass(0.2, 0.5, 4000) ==  pytest.approx(251.327, rel=1e-3)
        assert helpers.calculate_cylinder_mass(2.75, 0.01, 0.5) ==  pytest.approx(0.1188, rel=1e-3)

    def test_calculate_cylinder_moment_of_inertia(self):
        """
        Test cylinder moment of inertia calcs.
        Examples taken from:
        http://hyperphysics.phy-astr.gsu.edu/hbase/icyl.html
        """
        assert helpers.calculate_cylinder_moment_of_inertia(2, 1200) == pytest.approx(2400, rel=1e-3)
        assert helpers.calculate_cylinder_moment_of_inertia(12E-2, 0.6) == pytest.approx(0.00432, rel=1e-3)

    def test_calculate_hollow_section_mass(self):
        """
        Test calculation of hollow section mass.
        Examples taken from:
        https://dreamcivil.com/weight-of-rectangular-hollow-section-steel/
        """
        assert helpers.calculate_hollow_section_mass(1, 50E-3, 25E-3, 3E-3, 7850) == pytest.approx(3.25, rel=1e-3)
        assert helpers.calculate_hollow_section_mass(1, 50E-3, 25E-3, 3E-3, 7850) == pytest.approx(3.25, rel=1e-3)

    def test_calculate_rad_second_from_rpm(self):
        """
        Tests conversion from rpm to rad/s.
        Examples taken from:
        https://www.convertunits.com/from/RPM/to/rad/sec
        """
        assert helpers.calculate_rad_second_from_rpm(0) == pytest.approx(0)
        assert helpers.calculate_rad_second_from_rpm(450) == pytest.approx(47.12388975)
        assert helpers.calculate_rad_second_from_rpm(1200) == pytest.approx(125.66370599999999)

    def test_calculate_rpm_from_rad_second(self):
        """
        Tests conversion from rad/s to rpm.
        Examples taken from:
        https://www.convertunits.com/from/rad/sec/to/rpm
        """
        assert helpers.calculate_rpm_from_rad_second(0) == pytest.approx(0)
        assert helpers.calculate_rpm_from_rad_second(47.12388975) == pytest.approx(450)
        assert helpers.calculate_rpm_from_rad_second(125.66370599999999) == pytest.approx(1200)

    def test_calculate_m_rad_from_mm_rotation(self):
        """
        Tests conversion of ballscrew pitch from mm/rev to m/rad.
        Examples taken from:
        https://www.google.com/search?q=mm+per+rev+to+m%2Frad
        """
        assert helpers.calculate_m_rad_from_mm_rotation(0) == pytest.approx(0)
        assert helpers.calculate_m_rad_from_mm_rotation(1) == pytest.approx(0.000159155)
        assert helpers.calculate_m_rad_from_mm_rotation(4) == pytest.approx(0.00063662)

    def test_calculate_mm_min_from_m_s(self):
        """
        Tests conversion of speed in m/s to mm/min.
        Examples taken from:
        https://www.google.com/search?q=m%2Fs+to+mm%2Fmin
        """
        assert helpers.calculate_mm_min_from_m_s(0) == pytest.approx(0)
        assert helpers.calculate_mm_min_from_m_s(1) == pytest.approx(60000)
        assert helpers.calculate_mm_min_from_m_s(4.20) == pytest.approx(252000)

    def test_calculate_mm_min_min_from_m_s_s(self):
        """
        Tests conversion of acceleration in m/s^2 to mm/min^2.
        Examples taken from:
        https://www.google.com/search?q=m%2Fs%5E2+to+mm%2Fmin%5E2
        """
        assert helpers.calculate_mm_min_min_from_m_s_s(0) == pytest.approx(0)
        assert helpers.calculate_mm_min_min_from_m_s_s(1) == pytest.approx(3_600_000)
        assert helpers.calculate_mm_min_min_from_m_s_s(4.20) == pytest.approx(15_120_000)

    def test_calculate_mm_s_s_from_m_s_s(self):
        """
        Tests conversion of acceleration in m/s^2 to mm/s^2.
        Examples taken from:
        https://www.google.com/search?q=m%2Fs%5E2+to+mm%2Fs%5E2
        """
        assert helpers.calculate_mm_s_s_from_m_s_s(0) == pytest.approx(0)
        assert helpers.calculate_mm_s_s_from_m_s_s(1) == pytest.approx(1000)
        assert helpers.calculate_mm_s_s_from_m_s_s(4.20) == pytest.approx(4200)

    def test_calculate_screw_force(self):
        """
        Tests screw force calculation.
        Example taken from:
        https://physics.stackexchange.com/questions/283788/force-generated-by-ball-screw-linear-motor
        """
        test_lead = 8 # mm/rotation
        test_lead_metric = helpers.calculate_m_rad_from_mm_rotation(test_lead)
        assert helpers.calculate_screw_force(0.26, test_lead_metric, 1) == pytest.approx(204, rel=1e-3)

    def test_get_stepper_moment(self):
        """
        Tests lookup and linear interpolation of stepper moment.
        We'll first test going out of bound, then an exact match,a nd finally
        interpolation.
        Examples taken from:
        https://www.studysmarter.co.uk/explanations/math/statistics/linear-interpolation/
        """
        moment_table = {
            3: 2,
            7: 9
        }
        # test going out of bounds.
        with pytest.raises(OverflowError):
            helpers.get_stepper_moment(7.1, moment_table)
        with pytest.raises(OverflowError):
            helpers.get_stepper_moment(2.9, moment_table)
        # Exact match
        assert helpers.get_stepper_moment(3, moment_table) == pytest.approx(2)
        assert helpers.get_stepper_moment(7, moment_table) == pytest.approx(9)
        # Linear interpolation
        assert helpers.get_stepper_moment(5, moment_table) == pytest.approx(5.5)