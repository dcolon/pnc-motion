import pncmotion
import pncmotion.helpers as helpers
import math

################################################################################
# Config
################################################################################

# General
# Density of steel [kg/m^3]
steel_density: float = 8050

# Y-Axis #######################################################################

# General
# RPM to start caluclation from (used for interpretation of moment curve)
y_rpm_start = 450
# Number ofmotors on the y-axis.
y_stepper_count = 2

# Steppers
# Type of stepper. Used in output.
y_stepper_type: str = "ESS17-07"
# Url for stepper. Used in output.
y_stepper_url: str = "https://www.omc-stepperonline.com/ess-series-0-60nm-85oz-in-nema-17-integrated-closed-loop-stepper-servo-motor-24-36vdc-1000cpr-ess17-07"
# moment [Nm]
y_stepper_moment: float = 0.4 # @350 RPM
# Moment of inertia (rotor inertia) [kg*m^2]
y_stepper_moment_inertia: float = 1.1E-5
# Y-stepper moment curve. This will be used to calculate moment at a given rpm.
# Put it in here in the format Speed[RPM] -> moment [Nm]
# Ensure speed is ordered low to high.
# Ensure speed is an integer (whole) number.
# An error will be thrown if an attempt is made to look up moment above the
# highest speed. Make sure the highest entry matches the max rpm of your chosen
# stepper.
y_stepper_moment_table = {
    0: 0.6,
    150: 0.55,
    300: 0.5,
    450: 0.35,
    600: 0.275,
    750: 0.23,
    900: 0.2,
    1050: 0.17,
    1200: 0.12,
    1350: 0.1,
    3000: 0.1
}

# Pulleys
# Stepper pulley teeth count.
y_pulley_stepper_teeth_count: int = 24
# Ball screw pulley teeth count.
y_pulley_ball_screw_teeth_count: int = 24
# Pulley/belt efficiency.
y_pulley_efficiency: float = 0.98 # https://www.pfeiferindustries.com/timing-belt-advantages-and-disadvantages

# Ball screw
# Type, used for output.
y_ball_screw_type: str = "1204"
# Lead [mm/rotation]
y_ball_screw_lead: float = 4
# Length, used for moment of inertial calcs. [m]
y_ball_screw_length: float = 0.51
# Diameter, used for moment of inertial calcs. [m]
y_ball_screw_diameter: float = 1212E-3
# Density of ballscrew, used for moment of inertial calcs. [kg/m^3]
y_ball_screw_density: float = steel_density # Steel
# Y ballscrew efficiency
y_ball_screw_efficiency: float = 0.9 # https://monroeengineering.com/blog/pros-and-cons-of-using-ball-screws

# Y rollers
# Gantry steel length [m]
y_roller_steel_length: float = 120E-3
# gantry steel width [m]
y_roller_steel_width: float = 50E-3
# gantry steel height [m]
y_roller_steel_height: float = 50E-3
# gantry steel thickness [m]
y_roller_steel_thickness: float = 3E-3
# Density of steel, used forweight calcs. [kg/m^3]
y_roller_steel_density: float = steel_density # Steel
# Number of Y-rollers.
y_roller_count = 2

# Gantry
# Gantry steel length [m]
gantry_steel_length: float = 535E-3
# gantry steel width [m]
gantry_steel_width: float = 50E-3
# gantry steel height [m]
gantry_steel_height: float = 50E-3
# gantry steel thickness [m]
gantry_steel_thickness: float = 3E-3
# Density of steel, used forweight calcs. [kg/m^3]
gantry_steel_density: float = steel_density # Steel

# Spindle
# Spindle type. Used in output.
spindle_type: str = "G-Penny 2.2kW ER20"
# Spindle url. Used in output.
spindle_url: str = "https://www.aliexpress.com/item/32853195470.html"
# Spindle mass [kg]
spindle_mass: float = 6.5

# Mass of all the other hardware on the x-axis (estimate for now.) [kg]
x_axis_hardware_mass: float = 10

################################################################################
# Functions.
################################################################################

# General



################################################################################
# Calculations.
################################################################################

def main():
    print(f"Starting pncmotion v{pncmotion.__version__}")

    # Calculate force throught the system.
    # Stepper -> pulleys -> ballscrew -> gantry

    # Calculate our start rotaional velocity.
    y_rotational_velocity = helpers.calculate_rad_second_from_rpm(y_rpm_start)

    # And convert pitch.
    y_ball_screw_lead_metric = helpers.calculate_m_rad_from_mm_rotation(y_ball_screw_lead)

    # And get current moment.
    y_stepper_moment: float = helpers.get_stepper_moment(helpers.calculate_rpm_from_rad_second(y_rotational_velocity), y_stepper_moment_table)

    # Then calculate moment on the ballscrew from the ratio between pulleys and
    # efficiency.
    y_ballscrew_moment = y_stepper_moment * (y_pulley_ball_screw_teeth_count / y_pulley_stepper_teeth_count) * y_pulley_efficiency

    # Calculate velocity.
    y_velocity = y_rotational_velocity * (y_pulley_stepper_teeth_count / y_pulley_ball_screw_teeth_count) * y_ball_screw_lead_metric

    # Then calculate thrust from the ballscrew(s).
    y_ballscrew_force = helpers.calculate_screw_force(y_ballscrew_moment, y_ball_screw_lead_metric, y_ball_screw_efficiency)
    #y_ballscrew_force = y_ballscrew_moment * 2 * math.pi / (y_ball_screw_lead_metric * y_ball_screw_efficiency)
    y_total_force = y_stepper_count * y_ballscrew_force

    # Calculate mass of everything being moved by y-axis.
    gantry_steel_mass = helpers.calculate_hollow_section_mass(gantry_steel_length, gantry_steel_width, gantry_steel_height, gantry_steel_thickness, gantry_steel_density)
    y_roller_steel_mass = helpers.calculate_hollow_section_mass(y_roller_steel_length, y_roller_steel_width, y_roller_steel_height, y_roller_steel_thickness, y_roller_steel_density)
    x_axis_mass = x_axis_hardware_mass + spindle_mass + gantry_steel_mass + y_roller_steel_mass

    # And then work out maximum acceleration.
    y_acceleration = y_total_force / x_axis_mass

    ################################################################################
    # Output.
    ################################################################################

    print(f"Y - X-axis mass:               {round(x_axis_mass, 2)} [kg]")
    print(f"Y - Stepper rpm:               {round(helpers.calculate_rpm_from_rad_second(y_rotational_velocity))} [RPM]")
    print(f"Y - Stepper moment:            {round(y_stepper_moment, 2)} [Nm]")
    print(f"Y - Ballscrew moment:          {round(y_ballscrew_moment, 2)} [Nm]")
    print(f"Y - Ballscrew force:           {round(y_ballscrew_force, 2)} [N]")
    print(f"Y - Total force:               {round(y_total_force, 2)} [N]")
    print(f"Y - Velocity:                  {round(helpers.calculate_mm_min_from_m_s(y_velocity), 2)} [mm/min]")
    print(f"Y - Max unloaded acceleration: {round(helpers.calculate_mm_s_s_from_m_s_s(y_acceleration), 2)} [mm/s^2]")

if __name__ == '__main__':
    main()
